# webhooks

Projects can request webhooks be fired when new images are pushed here.

To request a new webhook, create a confidential issue using the appropriate template.
