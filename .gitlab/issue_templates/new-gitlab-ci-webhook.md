# New Gitlab CI Webhook Request

Please mark this issue as confidential!

Webhook token: (Create one under Settings > CI/CD > Pipeline Triggers)

Webhook url: (Copy the webhook URL in Settings > CI/CD > Pipeline Triggers > Use Webhook)

Which ref would you like the pipline to trigger on?: (typically main)

Would you like a pipeline to be trigged on release candidates, releases, or both? (typically both)

Would you like any additional variables to be set other than `NEW_IMPLEMENTATION_RELEASE`/`NEW_IMPLEMENTATION_RELEASE_TAG` and/or `NEW_IMPLEMENTATION_RC`/`NEW_IMPLEMENTATION_RC_TAG` ?